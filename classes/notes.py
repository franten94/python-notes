﻿ 

1. Python naming conventions:

    a. For variable names, function names, object names, and module names use lower case separated by underscore, for example:

     my_router 
     find_set_of_devices
     convert_id_string_to_list

    b. For class names, capitalize the first letter of each word.  Do not use any underscores.  For example:

﻿﻿﻿     ManyToManyField
     ClientHistory
     UserProfile﻿﻿﻿ 

    c. For constants, use all upper case; use underscores for word separation.

     PI = 3.14
     EMAIL_MODE
     EMAIL_FROM_ADDRESS


2. Indentation: 

I stated on the video that indentation matters in Python, but I did not provide any examples. 
 Here is some example code showing you indentation.  We haven't talked about 'if conditionals' 
 so this code is just to show you an example of indentation.

>>>>> CODE <<<<<

a = 10
b = 20

if a == 10:
    print 'First level of indentation'

    if b == 20:
        print 'Second level of indentation'
        print 'Be consistent in your indentation'
        print 'Use four spaces for each level'

    print 'I am now out of the second if statement'

print 'I am now out of the first if statement'

>>>>> END <<<<<﻿

Note, the first if statement is terminated by a colon; after the colon you see some indented Python statements.  Everything at this first indentation level is part of the first block (i.e. it will be executed if the first 'if' statement is True).  

The second if statement starts a new block of code (and a new indentation level).  Once again the if statement is terminated by a colon.  

You indicate the end of each section (block of code) by ending the indentation.


3. Comments

I also didn't mention in the videos that you can indicate a comment by using the # character (all the characters after the pound sign are ignored).

# This is a comment

a = 10        # and so is this 


4. How to pipe <stdin> into a Python script (Linux and MacOS)

Python supports a way that you can pipe data into it (on Linux and MacOS).  You can then process the data you sent into it line-by-line (see below).

Note, some of these statements we haven't talked about yet (import and for loops). Consequently, if this doesn't make any sense, just skip this section.

>>>>> CODE (file test3.py) <<<<<

#!/usr/bin/env python

import fileinput

for line in fileinput.input():
    print line.split(".")

>>>>> END <<<<<

Here is an example using this script where I echo an IP address into it and then the IP address is split into its octets.

$ echo '192.168.1.1'  | ./test3.py 
['192', '168', '1', '1\n']