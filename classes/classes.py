
## Para convertir a strings:
a=4
a=str(a)
print type(a)

# Length of strings
vari='whatever'
len(vari)

ip_addr='192.168.1.1'
print "My IP address is: " + ip_addr

router_name='sf-rtr-1'

# Formateo de prints
## UPER cases pasar txto a mayusculas
a='this is hard rock'
b=a.upper()
print b
>>THIS IS HARD ROCK

print "Router (%s) has an IP address of (%s)" % (router_name,ip_addr)
>>Router (sf-rtr-1) has an IP address of (192.168.1.1)

# Tambien el % puede incluir strings para modificar el print
print "Router (%s) has an IP address of (%s), y %s es pavisimo " % (router_name,ip_addr, 'Python')
# Ampliar el ancho entre caracteres
# ejemplo 15 caracteres entre las variables
a = 10
b= 200
c= 65
print "%15s %15s %15s" (a,b,c)

# para especificar cuantos decimales saldran
pi=3.141561
# 2 decimales
print "%.2f" % pi # la f es para especificar de t
# 1 decimal
print "%.1f" % pi

# Otra manera es con el metodo de format
print "Router {0} has an ip address of {1}".format(router_name, ip_addr)

####  READ INPUT WITH RAW INPUT #####

ipaddr=raw_input("Enter IP address please:\n")
print "The ip address is: %s" % ipaddr 

# python solo entrega valores de tipo integer a menos que se le especifique

10/3 ## solo entrega un numero entero
>>3

11/3 ## Solo entrega el entero
>>3

# para que entregue el decimal hay que espeficar uno como decimal
11/3.0
>>3.6666666666666665
# Para conocer el cociente de una division
10 % 3
>>1
''' 10|_3__
	-1-	3 '''
11 % 3
>>2
12%3
>>0

# A veces imprime numeros con una L mayuscula significa que son largos
numerolargo=9999999999999999999999999999999999999999999999999
print numerolargo
type(numerolargo)

# Numeros exponenciales y floats
a=2.0
b=7.091
c=3.145e5
d=3.14e-2

# Numeros y binarios
ipaddress='172.16.1.254'
ipaddress.split('.')

### Hacia INTEGERS ###
ipaddress.split('.')[0]
>>'172'
# Para imprimirlo como integer
int(ipaddress.split('.')[0])
>> '172'

# Especificar que la base sea 2, y se devuelve a integer
firstIntoct=int(firstBinoctet,2)
print firstIntoct

# DE HEX a integer, en este caso es HEX la base es 16
firstIntoct2=int(firstHEXoct,16)

### Hacia BINARIOS ###
# Para convertir en binarios
bin(172)
>>'0b10101100'

# Con variables
firstBinoctet=bin(int(ipaddress.split('.')[0]))
print firstBinoctet
>>'0b10101100'

### Hacia HEX ###

firstHEXoct=hex(firstIntoct)
print firstHEXoct
>> '0xac'

# Tambien se puede importar la clase math de Python y asi obtener mas funciones matematicas
import math
dir(math)
# Ejemplo:
PI = math.pi
print PI
>> '3.14159265359'



''' LISTS AND TUPLES'''
##### LISTS ####
# Se crea la variable y se encierra entre brackets
list_ip= ['9','72','25']

# Las listas pueden incluir varios tipos de datos dentro de ellas:
# Aqui tienen enteros, strings floats y una listo dentro de otra
a=[207,'whatever',2.07,[]]

# Para acceder al primer elemento
a[0]
a[-1] # Para el ultimo

a[0]= 555 # Cambia el valor de index 0

len(a) # Tamano de la lista
>>4

b=[1,2,3]

c=a+b # Suma de listas, LIST CONCATENATION
print c
>>[555, 'whatever', 2.07, [], 1, 2, 3]

# MODIFICAR LISTAS
a.append('hello') # Para agregar al final de la lista una nueva variable
a.pop() # Elimina el ultimo valor de la lista
a.pop(3) # Para eliminar un valor en especifico de la lista
del a[0]
a.remove(555) # Buscara el index que coincida y lo eliminara de la lista

# CREAR RANGOS
range(10)  
>> [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
range(100,110)
>>[100, 101, 102, 103, 104, 105, 106, 107, 108, 109]

# para agarrar partes de listas
>>> a=range(200,207)
>>> a
[200, 201, 202, 203, 204, 205, 206]
>>> a[0:]
[200, 201, 202, 203, 204, 205, 206]
>>> a[0:6]
[200, 201, 202, 203, 204, 205]
>>> a[:]
[200, 201, 202, 203, 204, 205, 206]
>>> a[0:-1]
[200, 201, 202, 203, 204, 205]

# Se pueden asignar partes de listas a una variable
b=a[:]

#### TUPLES ####
# Los tuples son listas que no pueden ser modificables
# Los metodos de append,pop o delete no podran ser corridos
# porque no los acepta. Son listas que nunca cambian
t=(1,2,3,'whatever')
type(t)
<type 'tuple'>

#### BOOLEANS ######
>>> a=True
>>> a
True
>>> b=False
>>> a or b
True
>>> a and b
False
>>

###JOINS###
# El metodo join permite unir una lista de STRINGS a traves de el caracter que se le configure

a=["Hola","Como","estas",'4']
a=("--").join(a)
print a 

#### SI se quiere unir numeros hay que llevarlos a strings
a=str(range(10))
a=a.split() # Y hacerle debido split
a=("|").join(a)
print a 



#####FLOW CONTROL#####

## Comparison Operators:

== # equal than
!= # not equal
> # greater than
>= # equal or greater than
< # less than
<= # less or equal than

######IF ELSE-IF ELSE ############

a = int(raw_input("\nintroduzca el valor de a\n"))
b = int(raw_input("\ninsert B value\n"))
c = int(raw_input("\nInsert C value\n"))

if a == 1:
	print "You guessed A value and its %s " % a
	if c == 5:
		print "Yey you guessed A and C value. C is %s " % c
elif b == 3:
	print "Congrats that was the real B value (%s)" % b
else:
	print "\nYou are seeing this because A isnt 1, B isnt 3 and C isnt 5 :P\n"

#### una decision puede ser extensa:
a=10
b=20
c=30

if (a == 10) and (b == 20) and (c == 30):
	print 'good'
print 'outside of if '

##### For loops ####
# Basic structure
a = range(10)
for i in a:
	print i

# Para iterar sobre la lista
# Por ejemplo para ver los cuadrados de la lista A:
a = range (10)
# Defino i como variable al index a tratar:
i =0
for element in a:
     a[i]=element**2 # el index de a lo elevare al cuadrado
     i+=1 # Un contador para que vaya moviendo el indice de los elementos a buscar el cuadrado
print a
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]


## Funcion enumerate: Da (valor del index, valor de la lista)
>>> a=range(10)
>>> for test in enumerate(a):
...     print test
...
(0, 0)
(1, 1)
(2, 2)
(3, 3)
(4, 4)
(5, 5)
(6, 6)
(7, 7)
(8, 8)
(9, 9)

# Enumerate 2
>>> a='w h a t e v e r'
>>> a=a.split()
>>> for test in enumerate(a):
...     print test
...
(0, 'w')
(1, 'h')
(2, 'a')
(3, 't')
(4, 'e')
(5, 'v')
(6, 'e')
(7, 'r')

# Iterara sobre la funcion enumerate(a), y al valor de la izquierda lo llamara i, derecha element
## Con esto se ahorra el contador usado en ejemplos anteriores,
##  ya que el index sera i y este ya se define con la funcion enumerate. element seguira siendo la lista

for i,element in enumerate(a):
	a[i] = element**2

print a

## Para romper los loops, existen las funciones de continue y break:##


a= range (10)
for i in a:
	print i
	if i==6:
		continue
	print "hello world"
# El continue se salta la siguiente linea de codigo y sigue iterando el for:
# no imprimio Hello world cuando detecto 6.
>>
0
hello world
1
hello world
2
hello world
3
hello world
4
hello world
5
hello world
6
7
hello world
8
hello world
9
hello world

###### BREAKS:
a= range(10)
for i in a:
	print i
	if i==6:
		break
	print 'hello world'
>> # Una vez alcanzado el break el mismo se sale del loop for.
0
hello world
1
hello world
2
hello world
3
hello world
4
hello world
5
hello world
6

# Para hacer los loops y simplemente pasar por ellos, esta la funcion "pass"
a = range(10)
for i in a:
	pass

if True:
	pass

#### PASSING ARGUMENTS INTO A SCRIPT ###
# Esto lo que hace es que los valores con los que uno ejecuta el script o programa
# sean almacenados en python, para en un futuro poderlos usar en el codigo:

			##WINDOWS##
# Crear archivo.py:
# Importar la clase sys
import sys

# Utilizar el atributo sys.argv que representa los argumentos dados a un programa
if len(sys.argv) == 2:
	ip_addr = sys.argv.pop()
	print "The IP Address is %s" % ip_addr
else:
	print "You didn't enter 2 arguments"

# Y correrlo como python archivo.py 192.168.1.1
			###LINUX###
vi archivo.py
#!/usr/bin/env python
import sys
if len(sys.argv) == 2:
	ip_addr = sys.argv.pop()
	print "The IP Address is %s" % ip_addr
else:
	print "You didn't enter 2 arguments"
# Hacerlo ejecutable chmod 755 archivo.py
# Y correrlo como "./archivo.py 192.168.1.1" o "python archivo.py 192.168.1.1"

# Para abrir archivos y leerlos posteriormente en python (se abren por defecto solo lectura)
with open('libro1.txt') as a:
	pass

# Solo escritura
open('libro2.txt', 'w') as b:
# Lectura y escritura
open('libro2.txt', 'r+') as b:

# Multiples archivos a partir de python 2.7 se pueden abrir varios archivos
# en una sola linea
with open('libro1.txt') as a, open('libro2.txt') as b:

######## WHILE LOOPS ###############
#Se pueden salir de los while loops haciendo que la condicion
# del WHILE sea falsa (un contador) o con la sentencia break
i=0
while True:
	if i>=10
		break
	print "Hello",i
	i+=1

while i <= 10:
	print "Hello", i
	i+=1

### DICTIONARIES##
# Diccionario en blanco:
a={}
# Diccionario con valores:
# Un diccionario esta compuesto por keys
# cada key es unica, y el diccionario no los imprime
# en la manera en que son escritos.
a= {
	'name': 'sf-rtr-1',
	'ip_addr': '1.1.1.1',
	'serial_number': 'FTX0001',
	'os_version': '12.4.15T',
	}

# para llamar un key tipo un index se usa
# el nombre del diccionario ['key']:
a ['ip_addr']
>>'1.1.1.1'
a.get('ip_addr')
>> '1.1.1.1'
## pero si el valor que se hace get, no existe el get retorna NONE (no se ven en el shell porque es none)
a.get('interface')
>>

# para crear un key se hace de la siguiente manera
a ['model'] = '2911'
# para eliminar una key
del a['model']

# para saber si una key esta contenida en un diccionario:
'ip_addr' in a
>> True

# para saber solo los keys de un diccionario:
print a.keys()
# para saber solo los valores de los keys del diccionario
print a.values()
# para ver una lista entre keys y values esta el metodo .items:
print a.items()
# se importa el modulo de pprint para dar un mejor formato a los prints
import pprint # o "from pprint import *" Y para imprimir solo se escribe pprint
# es bastante usable hacer un for por cada metodo de los diccionarios, en este caso es un for para los items
for k,v in a.items():
	pprint.pprint (k)
	pprint.pprint (v)
### HANDLING EXCEPTIONS ####
a = {}
b = []
# Se agrega el key name al diccionario a
a['name']
try:
	print 'Hello World'
	print a['name']
except KeyError as e:
	print "There was a key exception"
except IndexError as e:
	print str(e)
# esta el 
# except: "Que exceptua todas los errores incluso el de teclado, y no se puede dar ctrl+c para terminar un programa"
# except Exception: " que exceptua todos los errores menos el de teclado"

######## FUNCTIONS #################
# se crean con el keyword def, de definition of a FUNCTIONS

def hello_world():
	print "Hello World!"

# y se ejecuta con solo poner el nombre
hello_world()
# por defecto las funciones returnean none
# pero se les puede modificar para que returneen un valor
# o una sentencia booleana etc. etc. UN return es para que se ejecute la funcion
# y al finalizar devuelva este valor. Util para combinarlo con conditionals

def hello_world():
	print "Hello World!"
	return True

# Para obtener ayuda de las funciones se utiliza help(function_name)
# Y muestra un comentario añadido. Para añadir un comentario a una funcion propia

def hello_world():
	'''
	Esto es una funcion que ejecuta un Hola Mundo! y devuelve un valor de True
	'''
	print "Hello World!"
	return True

help (hello_world)'''
>>
Help on function hello_world in module __main__:

hello_world()
    Esto es una funcion que ejecuta un Hola Mundo! y devuelve un valor de True
'''
## Funciones con parametros/argumentos:
def a_sum(x,y):
	return x+y
## Se llama la funcion y cada valor se le mapea a los argumentos definidos en la funcion (x,y)
a_sum(5, 10)
	>> 15
a_sum('stri', 'ng')
	>> string
## Tambien se pueden mapear los argumentos explicitamente:
a_sum(x=10, y=5)
	>> 15

### Al momento de definir una funcion se puede especificar cual sera el valor por defecto de un argumento.
>>> def hello_world(x,y,z=100):
	return x+y+z

>>> hello_world(1,50)
151

 ### Variables dentro de las funciones o namespaces:
# Crear un archivo test.py y copiar y pegar lo siguiente:

x = 10
y = 20
z = 30

def simple_func():

 	x=100
 	y=200

 	print x
 	print y
 	print z

simple_func()

print x
print y
print z
# Al correrlo, se puede ver como python primero busca los valores de las variables dentro de  la funcion y luego del global module

>>> python test.py
100
200
300
10
20
30

## Otro ejemplo una funcion dentro de otra funcion:
vi test.py
x = 10
y = 20
z = 30
# Se define la primera funcion, luego se le asignaran valores a las variables x y
def simple_func():
	# Se define una segunda funcion
	def simple_func2():
		x =1000
		print x
		print y
		print z

 	x=100
 	y=200
 	print x
 	print y
 	print z
 	# Llamado de la segunda funcion luego de imprimir los xyz de la primera
 	simple_func2()
# llamada a la primera funcion
simple_func()
# ultimos valores a imprimir fuera de las funciones:
print x
print y
print z

>>> python test.py 
100
200
30
1000
200
30
10
20
30

## Variables dentro de las funciones parte2
def f1(una_lista):
	una_lista.append('whatever')

a=range(10)
f1(a)
print a

>>[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'whatever']


## Ejemplo 2, como la lista es de numeros esta puede cambiar de valores dentro de las funciones

def f2(una_lista):
	una_lista = []
	una_lista.append('something')
	print una_lista

a =range (10)
f2(a)
>>['something']
a
>>
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

# Ejemplo 3, el argumento sera un string. Un string es unmutable, es decir no cambia de valor a lo largo de la ejecucion del programa:

def f3(un_string):
	un_string = 'whatever'
	print un_string
# Al llamar la funcion f3 y pasarle el argumento, este no puede ser modificado ya que es un string- y por eso imprime - whatever
f3('Hola Mundo!')
>>whatever

######### IMPORT and FROM module IMPORT ###
# Los modulos que son importados no son mas 
# que simplemente unos archivos.py si no los 
# encuentra en la carpeta local donde se
# corrio python, los buscara en la raiz donde python fue instalado.

 # Ejemplo creamos un test.py
def a_sum(x,y):
	return x+y
# Lo guardamos en cualquier carpeta y ahi mismo abrimos python (cd Desktop ó cd Francesco\Desktop\)
import test
test.a_sum(5,16)
>>21

## From import
from test import a_sum
# Se puede ver como la funcion importada ahora aparece en el directorio, hay que tener cuidado con las funciones para que cada una sea unica y no choquen los nombres(?)
>> dir()
['__builtins__', '__doc__', '__name__', '__package__', 'a_sum']

a.sum(5,16)
>>21

### ABRIR ARCHIVOS####

f = open("show_ip_int_data.txt")
a = f.readlines()
print a
['Interface                  IP-Address      OK? Method Status                Protocol\n', 'GigabitEthernet0           172.31.64.6     YES NVRAM  up                    up      \n', 'Loopback0                  200.59.189.5    YES NVRAM  up                    up      \n', 'Loopback2                  200.59.189.7    YES NVRAM  up                    up      \n', 'NVI0                       200.59.189.5    YES unset  up                    up      \n', 'Virtual-Template1          200.59.189.5    YES unset  up                    down    \n', 'Vlan1                      172.31.111.6    YES NVRAM  up                    up      \n', 'Vlan2                      190.216.230.25  YES NVRAM  up                    up      \n', 'wlan-ap0                   172.31.111.6    YES unset  up                    up      \n']
# Para limpiar la memoria y volver al inicio del archivo
# y poder leerlo de nuevo
f.seek(0)

# Esta tambien la funcion readline
>>> f.readline()
'Interface                  IP-Address      OK? Method Status                Protocol\n'
>>> f.readline()
'GigabitEthernet0           172.31.64.6     YES NVRAM  up                    up      \n'
>>> f.readline()
'Loopback0                  200.59.189.5    YES NVRAM  up                    up      \n'
>>> f.readline()
'Loopback2                  200.59.189.7    YES NVRAM  up                    up      \n'
# Iterar sobre las lineas:
for line in f:
	print line
>>
Interface                  IP-Address      OK? Method Status                Protocol

GigabitEthernet0           172.31.64.6     YES NVRAM  up                    up      

Loopback0                  200.59.189.5    YES NVRAM  up                    up      

Loopback2                  200.59.189.7    YES NVRAM  up                    up      

NVI0                       200.59.189.5    YES unset  up                    up      

Virtual-Template1          200.59.189.5    YES unset  up                    down    

Vlan1                      172.31.111.6    YES NVRAM  up                    up      

Vlan2                      190.216.230.25  YES NVRAM  up                    up      

wlan-ap0                   172.31.111.6    YES unset  up                    up      

# Para eliminar la linea nueva creada por el print
>>> for line in f:
...     print line.strip('\n')
... 
Interface                  IP-Address      OK? Method Status                Protocol
GigabitEthernet0           172.31.64.6     YES NVRAM  up                    up      
Loopback0                  200.59.189.5    YES NVRAM  up                    up      
Loopback2                  200.59.189.7    YES NVRAM  up                    up      
NVI0                       200.59.189.5    YES unset  up                    up      
Virtual-Template1          200.59.189.5    YES unset  up                    down    
Vlan1                      172.31.111.6    YES NVRAM  up                    up      
Vlan2                      190.216.230.25  YES NVRAM  up                    up      
wlan-ap0                   172.31.111.6    YES unset  up                    up   


# Par abrir archivos hay 2 metodos, esta el que overwritea un archivo y lo pone en blanco,
# y otro que appendea el archivo:

# Este es el primero que lo abre como solo escritura, si el archivo existe lo reemplaza y lo pone en blanco
f=open("newfile","w")
f.write("new line\n")
f.close()

[francesco@localhost Desktop]$ cat newfile 
new line

# Este abre el archivo como solo escritura tambien, pero si el archivo ya existe no lo reemplaza
# ni lo pone en blanco, solo escribe nuevas lineas al final:

f=open("newfile","a")
f.write("other new line\n")
f.write("other new line\n")
f.write("other new line\n")
f.write("other new line\n")
f.close()

[francesco@localhost Desktop]$cat newfile 
new line
other new line
other new line
other new line
other new line

# La manera mas recomendable de abrir los archivos es con el metodo with open, de esta manera
# los archivos se cierran automaticamente sin tener que colocar f.close()

with open ("newfile") as un_file:
	un_file.readline()
	un_file.readline()
	un_file.readline()
# Para saber si el archivo esta cerrado-.
>>un_file.closed
True


### REGULAR EXPRESSIONS, PARA BUSCAR DENTRO DE VARIABLES STRINGS O VALORES:
import re
f = open ("show_ip_int_data.txt")
a=f.readline()
a=f.readline()


# Es super aconsejable utilizar raw strings en regex ya que los backslashes tienen importancia 
# Buscara en a, algo que empiece que incluya up
a_var = re.search(r"up", a)

# si el valor se encuentra, la funcion re.search devuelve un match, si no lo encuentre devuelve None

a_var
>><_sre.SRE_Match object at 0xb77787c8>

# Se puede hacer un if, para saber si un
# string esta contenido con:

if a_var:
	print "MATCHED!"
# Muestra lo que hizo match en este caso up pq es lo que se especifico en la sentencia search()
a_var.group()
'up'

######### IMPORTING MODULES #####
## Cuando se hace un import siempre se ejecuta todo el codigo del archivo a impoortar
# solo se ejecuta una vez pero es importante ya que se ejecuta completamente
#vi test.py
a=100
print "Hello"

def some_func():
	print "This is inside of some func"

print "World"

####################################

>>> import test
Hello
World
>>>
>>>
>>>
>>> test.some_func()
This is inside of some func
>>>test.a
100
>>> test.b=77

>>>
>>>
>>> from test import *
Hello
World
>>>
>>>
>>> some_func()
This is inside of some func
>>>
>>>
>>> a
100
>>>
>>> exit()

C:\Users\Fran\Desktop>python
Python 2.7.11 (v2.7.11:6d1b6a68f775, Dec  5 2015, 20:40:30) [MSC v.1500 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
>>>
>>> import test as zz
Hello
World
>>> a
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'a' is not defined
>>> zz.a
100


#Cada archivo tiene su propio namespace, cada variable definida en los MODULOS sera diferente
# de las variables creadas globalmente

# Cada vez que se crea un modulo se puede diferenciar si esta siendo IMPORTADO o si esta siendo ejecutado
# Gracias a una variale llamada __name__

#vi test.py
print __name__

def some_func():
	print "inside some funcion"

### SE PUEDE OBSERVAR QUE CUANDO SE EJECUTA ESTE MODULO
## LA SALIDA DE LA VARIABLE __name__ es __main__

# C:\Users\Fran\Desktop>python test.py
__main__

# Pero cuando se importa directamente a la shell
# la salida de la variable __name__ es igual al nombre del modulo
# en este caso test >>

# C:\Users\Fran\Desktop>python
Python 2.7.11 (v2.7.11:6d1b6a68f775, Dec  5 2015, 20:40:30) [MSC v.1500 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import test
test
>>>

### con esta variable se puede definir un IF para saber si el modulo esta siendo importado o ejecutado de la shell

if __name__==__main__:
	# Este codigo sera ejecutao si el modulo es ejecutado externamente
	print "do something"
else:
	# Este codigo sera ejecutado si el modulo es importado en la shell:
	print "Imported"

 
### PACKAGES ######

>>> import sys
>>>
>>> print sys.path
['', 'C:\\Windows\\SYSTEM32\\python27.zip', 'C:\\Python27\\DLLs', 'C:\\Python27\\lib', 
'C:\\Python27\\lib\\plat-win', 'C:\\Python27\\lib\\lib-tk', 'C:\\Python27', 'C:\\Python27\\lib\\site-packages']

## Para crear un paquete solo es necesario crear una carpeta y en esa carpeta crear un archivo llamado __init__.py
## Una vez creado este archivo en la carpeta, ese necesario agregar al PYTHONPATH la carpeta creada
## Y de esta manera se pueden importar o ejecutar los paquetes.... 
## La diferencia de un PAQUETE con un MODULO, es que un paquete contiene muchos MODULOS dentro de el guardados en una carpeta.
## Cada vez que un paquete es importadom, este ejecuta cada uno de los modulos incluidos en su respectiva carpeta.

##############CLASSES AND OBJECTS#############################

##Las clases son como un blueprint para crear objetos y metodos

# Para entender las clases primero un ejemplo de una funcion para crear dispositivos
##esta es la manera de hacerlo sin crear una clase, es definiendo una func
# y creando un diccionario dentro de el (pasandoles los argumentos en parantesis)
# y creando sus respectivos keys:
def create_network_device(ip,username,password):
	net_dev = {}
	net_dev["ip"]=ip
	net_dev["username"]=username
	net_dev["password"]=password
	return net_dev

## Las clases se crean con el keyword class seguido del nombre y un parentesis
## en el parentesis se especifica de donde se inheritara esta nueva clase
## la que se utiliza por defecto es object:

class NetworkDevice(object):
	pass

# Recreando el primer ejemplo de la funcion + el diccionario:
>>> class NetworkDevice(object):
	## __init__ Es una funcion especial en python y se invoca automaticamente al crear un objeto
	## En las clases las funciones son llamadas METODOS
	## El primer argumento de este metodo (self) python lo pasa automaticamente como el nombre del objeto	
		def __init__(self,ip,username,password):
			self.ip=ip
			self.username=username
			self.password=password
# Aqui se nota como no hay problema en pasar de primero la IP
# ya que el argumento "self" ya se paso usando el nombre del objeto creado (rtr3)
# Se crea el objeto llamando a la clase y pasandole valores a los argumentos de __init__
>>> rtr3=NetworkDevice("1.1.1.1","ftenerelli","p@ssw0rd")
>>> rtr3.ip
'1.1.1.1'

### Las clases tambien se pueden basar en clases creadas anteriormente (clases dentro de clases).
## Ejemplo: se crearan dos clases (SomeClass y NewClass):

>>> class SomeClass(object):
...     def __init__(self,x,y):
...             self.x=x
...             self.y=y
...     def una_suma(self):
...             return self.x + self.y
...     def una_multiplicacion(self):
...             return self.x * self.y
...
>>> a=SomeClass(3,7)
>>> a.x
3
>>> a.y
7
>>> a.una_suma()
10
>>> a.una_multiplicacion()
21

### Al crear la nueva clase siempre buscara el metodo __init__ localmente,
## de no tener algun __init__ creado automaticamente lo buscara en la clase
## en la cual se esta basando
>>> class NewClass(SomeClass):
...     pass
...
## es por eso que al crear un objeto vemos como los argumentos son pasados al __init__ 
## de la primera clase
>>> b=NewClass(8,9)
>>> b.x
8
>>> b.y
9
>>> b.una_suma()
17
>>> b.una_multiplicaion()
72
### Creando de nuevo la NewClass, para que a su vez llame a los init de la anterior y poder
## utilizar los metodos correctamente
>>> class NewClass(SomeClass):
		#Como en esta clase ya hay un init NO IRA AUTOMATICAMENTE AL init de la anterior
...     def __init__(self,x,y,z):
...             self.z=z
				# Para llamar init de la clase basada es necesario especificarlo de la siguiente manera
...             SomeClass.__init__(self,x,y)
...     def una_suma(self):
...             return self.x + self.y + self.z
...
>>> joder_tio = NewClass(8,9,10)
## Como x,y no estan en NewClass son llamados con el init anterior SomeClass.__init__(self,x,y)
>>> joder_tio.x
8
>>> joder_tio.y
9
>>> joder_tio.z
10
## Como ya existe un metodo llamado una_suma() en NewClass, python lo buscara primero en NewClass
>>> joder_tio.una_suma()
27
## De no existir el metodo lo buscara de la clase basada
>>> joder_tio.una_multiplicaion()
72

##Una Clase dentro de otra puede ser util cuando por ejemplo hay una clase llamada Routers
## Que se crea de una manera bastante estandar, a su vez tendra 2 subclases
## una llamada Cisco otra HP, de esta manera, las subclases pueden llamar varios metodos de la 
## clase primaria sin tener que ser recreados nuevamente y sin perjudicarse unos con otros.

## Supongamos que Para las IP, usuarios y las passwords se podrian utilizar de la misma forma
## en routers Cisco y HP y la manera de conectarse(por SSH). PERO para entrar en modo privilegiado
## cambiarian unos de otros:

class Routers(object):
    def __init__(self,ip,username,password):
        self.ip=ip
        self.username=username
        self.password=password
    def connect_to_device(self):
        print "ssh username@p@ssw0rd"
    def enable_mode(self):
        print "generic"
class Cisco(Routers):
    def enable_mode(self):
        print "enable"
class HP(Routers):
    def enable_mode(self):
        print "enable terminal"