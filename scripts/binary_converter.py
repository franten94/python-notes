#!/usr/bin/env python
'''This script allows you to enter an IP address (in a valid decimal form), and convert it to binary'''
import sys
if len(sys.argv) != 2:
    # Exit the script
    sys.exit('''Too much arguments - Usage: \n Linux: ./binary_converter.py <ip_address>
    	\n Windows: python binary_converter.py <ip_address>''')

ip_addr = sys.argv.pop()
octets = ip_addr.split('.')

if len(octets) == 4:
    binary_octets=[]
    
    for octet in octets:        
        bin_octet = bin(int(octet))
        # Le meto un slice al al string de numeros
        # Para eliminar el 0b despues de la conversion
        bin_octet = bin_octet[2:]
		# Hago un mientras que los octetos no sean 8 numeros binarios le agrego un 0 a la izquierda
        while len(bin_octet)!=8:
        	bin_octet = '0' + bin_octet            
        # Se va agregando al final de la lista cada octeto en binario
    	binary_octets.append(bin_octet)
    # Se compacta la lista hacia una sola divida por un punto
    binary_octets = (".").join(binary_octets)
    
    print "\n%-20s %-50s" % ("IP address", "Binary")
    print "%-20s %-50s" % (ip_addr, binary_octets)
else:
    sys.exit("\nThe ip address is incorret, it doesn't have 4 octets")
