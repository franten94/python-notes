#!/usr/bin/env python
from requests import get

ip = get('http://api.ipify.org').text
print '\nMy public IP address is:\n%s\n\n' % ip
