#!/usr/bin/env python


print "\n\nConteste la pregunta segun lo necesitado"

cliente=raw_input("""\n1.)Nombre del cliente a ingresar (en minusculas y separado \
por guion en caso necesario)\nEjemplo: cisco-oficina\n""")

bandwidth=raw_input("\n2.)Ancho de Banda en Kbps. Ejemplo para 1Mbps: 1024\n")

public_ip=raw_input("\n3.)Direccion IP publica:\n")

subnet_mask=raw_input("""\n4.)Mascara de subred. Si es una sola IP (/32) presione enter, \
de lo contrario colocar la mascara.\nEjemplo: 255.255.255.248:\n""")

if subnet_mask:
	print "\nip access-list extended %s" % (cliente)
	print "permit ip any %s %s \nexit" % (public_ip,subnet_mask)
	print "\nclass-map match-any %s \nmatch access-group name %s\nexit" % (cliente,cliente)
	print "\npolicy-map regulacion-ab"
	print "class %s" % (cliente)
	print """police cir %s000 bc 512000 be 512000 conform-action transmit  exceed-action drop \
violate-action drop" % (bandwidth)"""
	print "exit\nexit\nexit"
else:
	print "\nip access-list extended %s" % (cliente)
	print "permit ip any host %s \nexit" % (public_ip)
	print "\nclass-map match-any %s \nmatch access-group name %s\nexit" % (cliente,cliente)
	print "\npolicy-map regulacion-ab"
	print "class %s" % (cliente)
	print """police cir %s000 bc 512000 be 512000 conform-action transmit  exceed-action drop \
violate-action drop""" % (bandwidth)
	print "exit\nexit\nexit"
