import paramiko
import time


def disable_paging(myconn):
    '''Para quitar el more despues de correr un comando show
        creo que esto es una funcion'''

    myconn.send("terminal length 0\n")
    time.sleep(1)

    # Clear the buffer on the screen
    output = myconn.recv(1000)

    return output


if __name__ == '__main__':


    # VARIABLES THAT NEED CHANGED
    ip='172.31.111.6'
    username='ftenerelli'
    password='Frante05'

    # Create instance of SSHClient object
    myconn_pre = paramiko.SSHClient()

    # Automatically add untrusted hosts 
    myconn_pre.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    # initiate SSH connection
    myconn_pre.connect(ip, username=username, password=password, look_for_keys=False, allow_agent=False)
    print "SSH connection established to %s" % ip

    # Use invoke_shell to establish an 'interactive session'
    myconn = myconn_pre.invoke_shell()
    print "Interactive SSH session established\n"

    # Strip the initial router prompt (creo que quita el banner)
    output = myconn.recv(1000)

    # See what we have (ver que queda en el buffer)
    print output

    # Turn off paging
    disable_paging(myconn)

    # Now let's try to send the router a command
    myconn.send("\n") #esto emula un enter
    myconn.send("show ip int brief\n")

    # Wait for the command to complete
    time.sleep(2)

    output = myconn.recv(5000)
    print output