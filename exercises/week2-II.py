'''3. Create an IP address converter (dotted decimal to binary):

    A. Prompt a user for an IP address in dotted decimal format.

    B. Convert this IP address to binary and display the binary result on
    the screen a binary string for each octet).

    Example output:
    first_octet    second_octet     third_octet    fourth_octet
    0b1010        0b1011000        0b1010         0b10011
'''
print "\n\nWelcome to the IP address converter\n"
print "Press enter to start"
raw_input()
dot_ip = raw_input("\nPlease enter an IP address\n")
split_dot = dot_ip.split(".")
firt_bin = bin(int(split_dot[0]))
second_bin = bin(int(split_dot[1]))
third_bin = bin(int(split_dot[2]))
fourth_bin = bin(int(split_dot[3]))

print "This is your IP address converted to binary, displayed octet by octet\n"

print "%20s %20s %20s %20s" % ('first_octet', 'second_bin', 'third_octet', 'fourth_octet')
print "%20s %20s %20s %20s" % (firt_bin, second_bin, third_bin, fourth_bin)
