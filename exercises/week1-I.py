'''Exercises:

Reference code for these questions is posted at:
https://github.com/ktbyers/pynet/tree/master/learnpy_ecourse/class2

Use the split method to divide the following IPv6 address
into groups of 4 hex digits (i.e. split on the ":")

FE80:0000:0000:0000:0101:A3EF:EE1E:1719    '''

ipaddress = 'FE80:0000:0000:0000:0101:A3EF:EE1E:1719'
print ipaddress
splitted = ipaddress.split(':')
print splitted
joined = ':'.join(splitted)
