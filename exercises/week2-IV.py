'''IV. You have the following string from "show version" on a Cisco router

cisco_ios = "Cisco IOS Software, C880 Software (C880DATA-UNIVERSALK9-M), Version 15.0(1)M4, RELEASE SOFTWARE (fc1)"

Note, the string is a single line; there is no newline in the string.

How would you process this string to retrieve only the IOS version:

   ios_version = "15.0(1)M4"'''

cisco_ios = "Cisco IOS Software, C880 Software (C880DATA-UNIVERSALK9-M),Version 15.0(1)M4, RELEASE SOFTWARE (fc1)"
cisco_ios=cisco_ios.split(",")
iosVersion=cisco_ios[2]
iosVersion=iosVersion.split(" ")
iosVersion=iosVersion[1]
print "\nThe IOS version of the router is: \n" + iosVersion