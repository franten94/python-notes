'''4. You have the following four lines from 'show ip bgp':

entry1 = "*  1.0.192.0/18   157.130.10.233        0 701 38040 9737 i"
entry2 = "*  1.1.1.0/24       157.130.10.233        0 701 1299 15169 i"
entry3 = "*  1.1.42.0/24     157.130.10.233        0 701 9505 17408 2.1465 i"
entry4 = "*  1.0.192.0/19   157.130.10.233        0 701 6762 6762 6762 6762 38040 9737 i"

Note, in each case the AS_PATH starts with '701'.

Using split() and a list slice, how could you process each of these such that--for each entry, you return an ip_prefix and the AS_PATH (the ip_prefix should be a string; the AS_PATH should be a list):

Your output should look like this:

ip_prefix             as_path                                           
1.0.192.0/18       ['701', '38040', '9737']                          
1.1.1.0/24           ['701', '1299', '15169']                          
1.1.42.0/24         ['701', '9505', '17408', '2.1465']                
1.0.192.0/19       ['701', '6762', '6762', '6762', '6762', '38040', '9737']

Ideally, your logic should be the same for each entry (I say this because once I teach you for loops, then I want to be able to process all of these in one four loop).

If you can't figure this out using a list slice, you could also solve this using pop().'''

entry1 = "*  1.0.192.0/18   157.130.10.233        0 701 38040 9737 i"
entry2 = "*  1.1.1.0/24       157.130.10.233        0 701 1299 15169 i"
entry3 = "*  1.1.42.0/24     157.130.10.233        0 701 9505 17408 2.1465 i"
entry4 = "*  1.0.192.0/19   157.130.10.233        0 701 6762 6762 6762 6762 38040 9737 i"

entry1=entry1.split()
entry1_preffix=entry1[1]
entry1_aspath=entry1[4:-1]

entry2=entry2.split()
entry2_preffix=entry2[1]
entry2_aspath=entry2[4:-1]

entry3=entry3.split()
entry3_preffix=entry3[1]
entry3_aspath=entry3[4:-1]

entry4=entry4.split()
entry4_preffix=entry4[1]
entry4_aspath=entry4[4:-1]

print "%-20s %-40s" % ('ip_preffix','AS_PATH')
print "%-20s %-40s" % (entry1_preffix,entry1_aspath)
print "%-20s %-40s" % (entry2_preffix,entry2_aspath)
print "%-20s %-40s" % (entry3_preffix,entry3_aspath)
print "%-20s %-40s" % (entry4_preffix,entry4_aspath)