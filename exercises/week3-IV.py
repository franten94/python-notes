'''
IV. Create a script that checks the validity of an IP address.  The IP address should be supplied on the command line.
    A. Check that the IP address contains 4 octets.
    B. The first octet must be between 1 - 223.
    C. The first octet cannot be 127.
    D. The IP address cannot be in the 169.254.X.X address space.
    E. The last three octets must range between 0 - 255.

    For output, print the IP and whether it is valid or not.
'''
import sys

if len(sys.argv) != 2:
    # Exit the script
    sys.exit('''Invalid number of arguments - Usage: \n Linux: ./binary_converter.py <ip_address>
    	\n Windows: python binary_converter.py <ip_address>''')

ip_addr = sys.argv.pop()
octets = ip_addr.split('.')
valid_ip = True
if len(octets) != 4:
	sys.exit ("\nCan't process IP Address %s\nUsage: week3-IV.py <x.x.x.x>" % ip_addr)

# Para cada uno de los octetos, crear dos variables un index (i) y cada octeto ej:(192)
# Este paso es para validar que la ip tenga 4 octetos y en un formato valido (x.x.x.x)
# Y tambien para convertir cada uno de los index de octets en ints. 
for i, octet in enumerate(octets):
	try:
		octets[i] = int(octet)
	except ValueError:
        # couldn't convert octet to an integer
		sys.exit("\n\nInvalid IP address: %s\n" % ip_addr)

# Se puede hacer con el index de los octetos. Ej: octets[0] o declarando una variable
# por cada item de la lista octets
first_octet, second_octet, third_octet, fourth_octet = octets
print type(first_octet)
if first_octet < 1: 
	valid_ip = False
elif first_octet > 223:
	valid_ip = False
elif first_octet == 127:
	valid_ip = False
elif (first_octet == 169) and (second_octet == 254):
	valid_ip = False
for octet in (second_octet,third_octet,fourth_octet):
	if (octet < 0) or (octet > 255):
		valid_ip = False

if valid_ip:
	print "\nThe IP Address %s has been checked and is: \n VALID \n" % ip_addr
else:
	sys.exit("\nThe IP Address %s has been checked and is: \n NOT VALID \n" % ip_addr)
