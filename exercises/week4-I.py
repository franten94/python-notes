'''I. Prompt a user to input an IP address.  Re-using some of the code from class3, 
exercise4--determine if the IP address is valid.  Continue prompting the user to re-input an 
IP address until a valid IP address is input.
'''

while True:

	valid_ip = True
	ip_addr = raw_input("\nPlease enter a valid IP address\n")
	octets = ip_addr.split('.')

	if len(octets) != 4:
		valid_ip = False

	# convert octet from string to int
	for i, octet in enumerate(octets):
		try:
			octets[i] = int(octet)
		except ValueError:
			# couldn't convert octet to an integer
			valid_ip = False

	first_octet, second_octet, third_octet, fourth_octet = octets

	if first_octet < 1: 
		valid_ip = False
	elif first_octet > 223:
		valid_ip = False
	elif first_octet == 127:
		valid_ip = False
	elif (first_octet == 169) and (second_octet == 254):
		valid_ip = False
	for octet in (second_octet,third_octet,fourth_octet):
		if (octet < 0) or (octet > 255):
			valid_ip = False
			

	if valid_ip:
		break