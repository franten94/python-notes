'''
I. Create an IP address converter (dotted decimal to binary).  This will be similar to what we did in class2 except:

    A. Make the IP address a command-line argument instead of prompting the user for it.
        ./binary_converter.py 10.88.17.23

    B. Simplify the script logic by using the flow-control statements that we learned in this class.

    C. Zero-pad the digits such that the binary output is always 8-binary digits long.  Strip off the leading '0b' characters.  For example,

        OLD:     0b1010
        NEW:    00001010

    D. Print to standard output using a dotted binary format.  For example,

        IP address          Binary
      10.88.17.23        00001010.01011000.00010001.00010111


    Note, you might need to use a 'while' loop and a 'break' statement for part C.

        while True:
            ...
            break       # on some condition (exit the while loop)

    Python will execute this loop again and again until the 'break' is encountered. '''
#!/usr/bin/env python

import sys
if len(sys.argv) != 2:
    # Exit the script
    sys.exit('''Too much arguments - Usage: \n Linux: ./binary_converter.py <ip_address>
    	\n Windows: python binary_converter.py <ip_address>''')

ip_addr = sys.argv.pop()
octets = ip_addr.split('.')

if len(octets) == 4:
    binary_octets=[]
    
    for octet in octets:        
        bin_octet = bin(int(octet))
        # Le meto un slice al al string de numeros
        # Para eliminar el 0b despues de la conversion
        bin_octet = bin_octet[2:]
		# Hago un mientras que los octetos no sean 8 numeros binarios le agrego un 0 a la izquierda
        while len(bin_octet)!=8:
        	bin_octet = '0' + bin_octet            
        # Se va agregando al final de la lista cada octeto en binario
    	binary_octets.append(bin_octet)
    # Se compacta la lista hacia una sola divida por un punto
    binary_octets = (".").join(binary_octets)
    
    print "\n%-20s %-50s" % ("IP address", "Binary")
    print "%-20s %-50s" % (ip_addr, binary_octets)
else:
    sys.exit("\nThe ip address is incorret, it doesn't have 4 octets")
