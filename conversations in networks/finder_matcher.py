#!/usr/bin/env python
import sys
from netaddr import *
total_conversaciones=0
total_cantv=0
total_facebook=0


if len(sys.argv) == 2:
	total_bwidth = sys.argv.pop()
	
else:
	print "You didn't specify the Total Bandwidth"

with open('libro1.csv') as f:

	for conversacion in f:		
		total_conversaciones+=1
		source_ip,dest_ip = conversacion.split(",",1)
		source_ip=IPAddress(source_ip)
		dest_ip=IPAddress(dest_ip)
		with open('libro2.csv') as subnets:

			for network in subnets:
				network=network.strip()
				network=IPNetwork(network)

				if source_ip in network:
					total_cantv+=1
				else:
					pass
				if dest_ip in network:
					total_cantv+=1
				else:
					pass
					
		with open('libro3.csv') as fcbk_networks:

			for fcbk_network in fcbk_networks:
				fcbk_network=fcbk_network.strip()
				fcbk_network=IPNetwork(fcbk_network)
				
				if source_ip in fcbk_network:
					total_facebook+=1
				else:
					pass
				if dest_ip in fcbk_network:
					total_facebook+=1
				else:
					pass

		with open('ip_bod.csv') as bod_networks:

			for bod_network in bod_networks:
				bod_network=bod_network.strip()
				bod_network=IPNetwork(bod_network)

				if source_ip in bod_network:
					total_bod+=1
				else:
					pass
				if dest_ip in bod_network:
					total_bod+=1
				else:
					pass		

porcentaje_cantv = (float(total_cantv)*100)/float(total_conversaciones)
trafico_cantv = porcentaje_cantv*float(total_bwidth)/100

porcentaje_fcbk = (float(total_facebook)*100)/float(total_conversaciones)
trafico_fcbk = porcentaje_fcbk*float(total_bwidth)/100

print "\nDe {0} conversaciones {1} son destinadas a Cantv".format(total_conversaciones,total_cantv)
print "Representando {0:.2f} % del trafico total\n".format(porcentaje_cantv)
print "Y un consumo de {0:.2f} Mbps de ancho de banda\n\n".format(trafico_cantv)

print "De {0} conversaciones {1} son destinadas a Facebook".format(total_conversaciones,total_facebook)
print "Representando {0:.2f}% del trafico total\n".format(porcentaje_fcbk)
print "Y un consumo de {0:.2f} Mbps de ancho de banda\n\n".format(trafico_fcbk)

print "De {0} conversaciones {1} son destinadas al BOD".format(total_conversaciones,total_bod)
print "Representando {0:.2f}% del trafico total\n".format(porcentaje_bod)
print "Y un consumo de {0:.2f} Mbps de ancho de banda\n\n".format(trafico_bod)